provider "aws" {
  region = "us-east-1"
}

variable "image_id" {
  type = string
}

output "public_ip" {
  description = "List of public IP addresses assigned to the instances, if applicable"
  value       = aws_eip.beegfs-setup.public_ip

}

resource "aws_instance" "beegfs-setup" {
  ami           = var.image_id
  instance_type = "t2.micro"
   key_name     = "adityas" 

  tags = {
    Name = "beegfs setup client and server"
  }

  } 

resource "aws_eip" "beegfs-setup" {
  instance = aws_instance.beegfs-setup.id
  depends_on = [aws_instance.beegfs-setup]
  vpc      = true



connection {
    type = "ssh"
    user = "centos"
    host = aws_eip.beegfs-setup.public_ip
   private_key = file("/builds/aditya.shewale/test1/adityas.pem")
  }

provisioner "file" {
    source      = "benchmark.sh"
    destination = "/tmp/benchmark.sh"
  }
 
}

  























